/**
 * Created by jonas on 2015-09-13.
 */

GameState.prototype.create = function() {

	this.game.stage.backgroundColor = '#FFFFFF';

	this.game.physics.startSystem(Phaser.Physics.ARCADE);

	g_game.map = this.game.add.tilemap('map' + (g_game.currentLevel %2));
	g_game.map.addTilesetImage('level');
	g_game.layerCollision = g_game.map.createLayer('collision');
	g_game.layerObjects = g_game.map.createLayer('objectives');

	// set collision for map tiles 1-5
	g_game.map.setCollisionBetween(1, 5);
	g_game.map.setCollision([6, 11, 16], true, g_game.layerObjects);

	// Tell the layer to resize the game 'world' to match its size
	g_game.layerCollision.resizeWorld();

	g_game.friendlyUnits = this.game.add.group();
	g_game.player = new Unit(this.game, 2 * 48, 6 * 48, 'character');
	g_game.friendlyUnits.add(g_game.player);
	resetPlayer();

	g_game.crates = this.game.add.group();
	g_game.crates.enableBody = true;
	//g_game.map.createFromObjects('objects', 12 , 'crate', 0, true, false, g_game.crates);
	var crate = g_game.crates.create(500, 300, 'crate');
	this.game.physics.enable(crate, Phaser.Physics.ARCADE);
	crate.body.bounce.y = 0.1;
	crate.body.maxVelocityy = 200;
	crate.body.gravity.y = g_game.gravity;
	//g_game.crates.callAll('body.bounce.y', 0.1);
	//g_game.crates.callAll('body.maxVelocity', 200);
	//g_game.crates.callAll('body.gravity.y', g_game.gravity);


	var style = { font: "42pt Courier", fill: "#ffffff", align: "center", 'font-weight': 'bold' };

	g_game.timerText = this.game.add.text(this.game.world.centerX, 26, "0:05", style);
	g_game.timerText.anchor.set(0.5);
	g_game.timeStart = 0;

	var majorLevel = Math.floor(g_game.currentLevel / 6) + 1;
	var minorLevel = g_game.currentLevel % 5 + 1;

	var levelText = this.game.add.text(
		this.game.world.centerX,
		this.game.world.height-17,
		'Level: ' + majorLevel + '-' + minorLevel,
		style
	);
	levelText.anchor.set(0.5);
	levelText.tint = g_game.lastResult === 'lost' ? 0xff0000 : 0x00ff00;

	tweenTint(levelText, levelText.tint, 0xffffff, 2000);

	g_game.cursors = this.game.input.keyboard.createCursorKeys();

	g_game.jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

};

function loadLevel() {
	g_game.phaserGame.state.start('game');
}

function tweenTint(obj, startColor, endColor, time) {
	// create an object to tween with our step value at 0
	var colorBlend = {step: 0};

	// create the tween on this object and tween its step property to 100
	var colorTween = g_game.phaserGame.add.tween(colorBlend).to({step: 100}, time);

	// run the interpolateColor function every time the tween updates, feeding it the
	// updated value of our tween each time, and set the result as our tint
	colorTween.onUpdateCallback(function() {
		obj.tint = Phaser.Color.interpolateColor(startColor, endColor, 100, colorBlend.step);
	});

	// set the object to the start color straight away
	obj.tint = startColor;

	// start the tween
	colorTween.start();
}