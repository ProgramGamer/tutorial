/**
 * Created by jonas on 2015-09-13.
 */

GameState.prototype.preload = function() {
	this.game.load.tilemap('map0', 'assets/map0.json?' + Math.random(), null, Phaser.Tilemap.TILED_JSON);
	this.game.load.tilemap('map1', 'assets/map1.json?' + Math.random(), null, Phaser.Tilemap.TILED_JSON);

	this.game.load.image('level', 'assets/level.png');
	this.game.load.spritesheet('crate', 'assets/crate.png', 36, 36);

	this.game.load.spritesheet('character', 'assets/character.png', 30, 48);
	this.game.load.spritesheet('enemy', 'assets/enemy.png', 30, 48);
};