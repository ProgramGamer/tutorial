/**
 * Created by jonas on 2015-09-13.
 */

GameState.prototype.update = function() {

	var curTime = new Date().getTime();
	if (g_game.timeStart === 0) {
		g_game.timeStart = curTime;
	}
	var timePassed = curTime - g_game.timeStart;

	if (timePassed <= g_game.LEVEL_TIMEOUT) {
		g_game.timerText.text = formatTime(timePassed);
	}
	else {
		loseLevel();
	}

	//this.game.physics.arcade.collide(g_game.player, g_game.door, winLevel);
	this.game.physics.arcade.collide(g_game.friendlyUnits, g_game.layerCollision);
	this.game.physics.arcade.collide(g_game.crates, g_game.layerCollision);
	this.game.physics.arcade.collide(g_game.friendlyUnits, g_game.crates);
	this.game.physics.arcade.collide(g_game.friendlyUnits, g_game.layerObjects, objectCollision);
	this.game.physics.arcade.collide(g_game.crates, g_game.layerObjects, objectCollision);

	// Reset the x (horizontal) velocity
	g_game.player.body.velocity.x = 0;

	if (g_game.jumpButton.isDown && (g_game.player.body.onFloor() || g_game.player.body.touching.down) && this.game.time.now > g_game.jumpTimer) {
		g_game.player.jump();
		g_game.jumpTimer = this.game.time.now + g_game.JUMP_TIMEOUT;
	}
	if (g_game.cursors.left.isDown) {
		g_game.player.runDirection(-1);
	}
	else if (g_game.cursors.right.isDown)
	{
		g_game.player.runDirection(1);
	}
	else {
		g_game.player.runDirection(0);
	}

};

function formatTime(timePassed) {

	function twoDigit(num) {
		return num < 10 ? '0' + num : num;
	}

	var timeLeft = g_game.LEVEL_TIMEOUT - timePassed;
	var secs = Math.floor(timeLeft / 1000);
	var mins = Math.floor(secs / 60);

	return '' + mins + ':' + twoDigit(secs%60) + '.' + Math.floor((timeLeft%1000)/100);
}

function objectCollision(player, tile) {
	if (tile.index === 6) {
		// button
		g_game.map.removeTile(tile.x, tile.y, g_game.layerObjects);
		// change button graphic
		g_game.map.putTile(7, tile.x, tile.y, g_game.layerObjects);

		// open all doors for this button color
		var result = true;
		while (result) {
			result = g_game.map.searchTileIndex(16, 0, false, g_game.layerObjects);
			if (result) {
				g_game.map.removeTile(result.x, result.y, g_game.layerObjects);
			}
		}
	}
	else if (tile.index === 11) {
		// door
		winLevel();
	}
}

function resetPlayer() {

	g_game.player.reset(2 * 48, 6 * 48);
}

function winLevel() {
	g_game.lastResult = 'won';
	g_game.currentLevel++;
	loadLevel();

}

function loseLevel() {
	g_game.lastResult = 'lost';
	g_game.currentLevel = Math.max(0, g_game.currentLevel-1);
	loadLevel();

}