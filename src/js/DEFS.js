/**
 * Created by jonas on 2015-09-13.
 */


window.g_game = {
	baseWidth: 24*48,
	baseHeight: 15*48,

	gravity: 1400,
	hozMove: 160,
	vertMove: -560,

	jumpTimer: 0,
	shootTimer: 0,
	JUMP_TIMEOUT: 350,
	SHOOT_TIMEOUT: 550,

	currentLevel: 0,
	LEVEL_TIMEOUT: 15000
};