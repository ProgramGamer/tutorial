/**
 * Created by jonas on 2015-09-13.
 */

GameState.prototype.create = function() {

	this.game.stage.backgroundColor = '#FFFFFF';

	this.game.physics.startSystem(Phaser.Physics.ARCADE);

	g_game.map = this.game.add.tilemap('map' + g_game.currentLevel);
	g_game.map.addTilesetImage('level');
	//map.addTilesetImage('objects');
	g_game.layerCollision = g_game.map.createLayer('collision');
	g_game.layerObjects = g_game.map.createLayer('objects');

	// set collision for map tiles 1-5
	g_game.map.setCollisionBetween(1, 5);
	g_game.map.setCollision([6, 11, 16], true, g_game.layerObjects);

	// Tell the layer to resize the game 'world' to match its size
	g_game.layerCollision.resizeWorld();

	g_game.friendlyUnits = this.game.add.group();
	g_game.player = new Unit(this.game, 2 * 48, 6 * 48, 'character');
	g_game.friendlyUnits.add(g_game.player);
	resetPlayer();

	//this.game.camera.follow(g_game.player);

	g_game.cursors = this.game.input.keyboard.createCursorKeys();

	g_game.jumpButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

};

function loadLevel(level) {
	g_game.currentLevel = level % 2;

	g_game.phaserGame.state.start('game');
}