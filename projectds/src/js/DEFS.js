/**
 * Created by jonas on 2015-09-13.
 */


window.g_game = {
	baseWidth: 16*48,
	baseHeight: 9*48,

	gravity: 1400,
	hozMove: 160,
	vertMove: -560,

	jumpTimer: 0,
	shootTimer: 0,
	JUMP_TIMEOUT: 350,
	SHOOT_TIMEOUT: 550,

	currentLevel: 1
};