/**
 * Created by jonas on 2015-09-13.
 */

GameState.prototype.update = function() {

	//this.game.physics.arcade.collide(g_game.player, g_game.door, winLevel);
	this.game.physics.arcade.collide(g_game.friendlyUnits, g_game.layerCollision);
	this.game.physics.arcade.collide(g_game.friendlyUnits, g_game.layerObjects, objectCollision);

	// Reset the x (horizontal) velocity
	g_game.player.body.velocity.x = 0;

	if (g_game.jumpButton.isDown && g_game.player.body.onFloor() && this.game.time.now > g_game.jumpTimer) {
		g_game.player.jump();
		g_game.jumpTimer = this.game.time.now + g_game.JUMP_TIMEOUT;
	}
	if (g_game.cursors.left.isDown) {
		g_game.player.runDirection(-1);
	}
	else if (g_game.cursors.right.isDown)
	{
		g_game.player.runDirection(1);
	}
	else {
		g_game.player.runDirection(0);
	}

};

function objectCollision(player, tile) {
	if (tile.index === 6) {
		// button
		g_game.map.removeTile(tile.x, tile.y, g_game.layerObjects);
		// change button graphic
		g_game.map.putTile(7, tile.x, tile.y, g_game.layerObjects);

		// open all doors for this button color
		var result = true;
		while (result) {
			result = g_game.map.searchTileIndex(16, 0, false, g_game.layerObjects);
			if (result) {
				g_game.map.removeTile(result.x, result.y, g_game.layerObjects);
			}
		}
	}
	else if (tile.index === 11) {
		// door
		winLevel();
	}
}

function resetPlayer() {

	g_game.player.reset(2 * 48, 6 * 48);
}

function winLevel() {

	loadLevel(g_game.currentLevel + 1);

}